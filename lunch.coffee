request = require "request"
_ = require "underscore"
Q = require "q"

fullNames =
  american: "American Diner"
  coffeehouse: "CH Jyväshovi"
  elonen: "Elonen Innova"
  bricks: "OB Inn"
  shalimar: "Shalimar"
  trattoria: "Trattoria Aukio"

class Lunch

  find: (short) ->
    deferred = Q.defer()
    @restaurants.then (restaurants) ->
      restaurant = _(restaurants).find (r) ->
        r.name is fullNames[short]
      deferred.reject(new Error("Restaurant not found")) if not restaurant
      deferred.resolve(
        name: restaurant.name
        foods: restaurant.foods.map (food) ->
          food.description
        .join(", ")
      )
    deferred.promise      
      
  fetch: ->
    deferred = Q.defer()
    request 'http://jklfood.enymind.fi/index.json', (error, response, body) ->
      deferred.reject(new Error(error)) if error
      deferred.resolve(JSON.parse(body));
    deferred.promise
    
  constructor: ->
    @restaurants = @fetch()
    
module.exports = new Lunch()