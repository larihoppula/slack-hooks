Matti = require "./matti"
Lunch = require "./lunch"

express = require 'express'
bodyParser = require 'body-parser'

app = express()
app.use(bodyParser())

app.post "/matti", (req, res) ->
  res.json
    text: "#{req.body.user_name}: #{Matti.randomWisdom()}"

app.post "/lounas", (req, res) ->  
  restaurant = req.body.trigger_word.replace("!","")
  
  Lunch.find(restaurant).then (restaurant) ->
    res.json
      text: "#{restaurant.name}: #{restaurant.foods}"

server = app.listen 3000, ->
  console.log 'Listening on port %d', server.address().port